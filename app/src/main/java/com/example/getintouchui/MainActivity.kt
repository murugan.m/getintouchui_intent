package com.example.getintouchui

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val listSpiiner = findViewById<Spinner>(R.id.IntrestedInlist)
        val items = arrayOf("None","Investment", "Trading", "Business", "Feedback")
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, items)
         listSpiiner.setAdapter(adapter)


        val sendBtnAction :Button = findViewById(R.id.sendBtn)
        sendBtnAction.setOnClickListener {
            val intentNext = Intent(this,ThankYouActivity::class.java)
            startActivity(intentNext)

        }
    }
}